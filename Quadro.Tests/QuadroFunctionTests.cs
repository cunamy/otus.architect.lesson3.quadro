using System;
using System.Collections.Generic;
using Quadro.Calculate;
using Xunit;

namespace Quadro.Tests
{
    public class QuadroFunctionTests
    {
        [Fact(DisplayName = "3. для уравнения x^2+1 = 0 корней нет")]
        public void NoRoots()
        {
            // Arrange
            double a = 1;
            double b = 0;
            double c = 1;
            
            // Act
            var result = QuadroFunction.Solve(a, b, c);
            
            // Assert
            Assert.Empty(result);
        }
        
        [Fact(DisplayName = "5. для уравнения x^2-1 = 0 есть два корня кратности 1 (x1=1, x2=-1)")]
        public void TwoRoots1()
        {
            // Arrange
            double a = 1;
            double b = 0;
            double c = -1;
            double x1 = 1;
            double x2 = -1;
            
            // Act
            var result = QuadroFunction.Solve(a, b, c);
            
            // Assert
            Assert.Equal(2,result.Length);
            Assert.Contains(result, (f)=> Math.Abs(f-x1)<QuadroFunction.Eps);
            Assert.Contains(result, (f)=> Math.Abs(f-x2)<QuadroFunction.Eps);
        }
        
        [Fact(DisplayName = "7. x^2+2x+1 = 0 есть один корень кратности 2 (x1= x2 = -1)")]
        public void OneRoot()
        {
            // Arrange
            double a = 1;
            double b = 2;
            double c = 1;
            double expected = -1;
            
            // Act
            var result = QuadroFunction.Solve(a, b, c);
            
            // Assert
            Assert.Equal(2,result.Length);
            Assert.Contains(result, (f)=> Math.Abs(f-expected)<QuadroFunction.Eps);
        }
        
        [Fact(DisplayName = "9. коэффициент a не может быть равен 0. В этом случае solve выбрасывает исключение")]
        public void AIsZero()
        {
            // Arrange
            double a = -1e-10;
            double b = 0;
            double c = 1;
            
            // Act
            Action act = () => QuadroFunction.Solve(a, b, c);
            
            // Assert
            ArgumentException exception = Assert.Throws<ArgumentException>(act);
            Assert.Equal("Коэффициент a не может быть равен 0",exception.Message);
        }
        
        [Theory(DisplayName = "11. дискриминант тоже нельзя сравнивать с 0 через знак равенства")]
        [InlineData(1, 2, 1, -1)]
        [InlineData(3, -18, 27, 3)]
        [InlineData(4, -4, 1, 0.5)]
        public void OneRoots(double a,double b,double c, double x1)
        {
            // Arrange
            
            
            // Act
            var result = QuadroFunction.Solve(a, b, c);
            
            // Assert
            Assert.Equal(2,result.Length);
            Assert.Contains(result, (f)=> Math.Abs(f-x1)<QuadroFunction.Eps);
        }
        
        [Theory(DisplayName = "12. числа типа double, кроме не числовых")]
        [InlineData(double.NaN, 1, 1)]
        [InlineData(1, double.NaN, 1)]
        [InlineData(1, 1, double.NaN)]
        [InlineData(double.NegativeInfinity, 1, 1)]
        [InlineData(1, double.NegativeInfinity, 1)]
        [InlineData(1, 1, double.NegativeInfinity)]
        [InlineData(double.PositiveInfinity, 1, 1)]
        [InlineData(1, double.PositiveInfinity, 1)]
        [InlineData(1, 1, double.PositiveInfinity)]
        public void NoValues(double a,double b,double c)
        {
            // Arrange
            
            
            // Act
            Action act = () => QuadroFunction.Solve(a, b, c);
            
            // Assert
            ArgumentException exception = Assert.Throws<ArgumentException>(act);
            Assert.Equal("Коэффициент должен быть числом",exception.Message);
        }
        
    }
}
