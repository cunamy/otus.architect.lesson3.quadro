﻿using System;

namespace Quadro.Calculate
{
    public class QuadroFunction
    {
        public static readonly double Eps = 1.0e-8; 
        public static double[] Solve(double a, double b, double c)
        {
            if(!(Double.IsFinite(a) && Double.IsFinite(b) && Double.IsFinite(c))) throw new ArgumentException("Коэффициент должен быть числом");
            
            if (Math.Abs(a) < Eps) throw new ArgumentException("Коэффициент a не может быть равен 0");
            
            var d = b * b - 4 * a * c;
            
            if (d < -Eps) return Array.Empty<double>();
            if (d > Eps) return new[] { (-b+Math.Sqrt(d))/(2*a), (-b-Math.Sqrt(d))/(2*a) };
                
            return new[] { -b/(2*a), -b/(2*a) };
        }
    }
}
